package com.artivisi.training.microservices.invoice.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class InvoiceRequest {
    private String salesNumber;
    private LocalDateTime salesTime;
    private String customerName;
    private String customerEmail;
    private BigDecimal amount;
    private String description;
}
